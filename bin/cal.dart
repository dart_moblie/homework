import 'package:cal/cal.dart' as cal;

class calculator {
  double x = 0;
  double y = 0;

  calculator(double x, double y) {
    this.x = x;
    this.y = y;
  }
  double add() {
    this.x = x;
    this.y = y;
    return x + y;
  }

  double substract() {
    this.x = x;
    this.y = y;
    return x - y;
  }

  double multiply() {
    this.x = x;
    this.y = y;
    return x * y;
  }

  double divide() {
    this.x = x;
    this.y = y;
    return x / y;
  }

  double modulo() {
    this.x = x;
    this.y = y;
    return x % y;
  }

  double exponent() {
    this.x = x;
    this.y = y;
    double exp = x;
    double res = 1;
    for (int i = 0; i < y; i++) {
      res = res * exp;
    }
    return res;
  }
}

void main() {
  calculator a = new calculator(6, 3);
  double sum = a.add();
  double substract = a.substract();
  double divide = a.divide();
  double modulo = a.modulo();
  double exponent = a.exponent();

  print('sum = $sum');
  print('substract = $substract');
  print('divide = $divide');
  print('modulo = $modulo');
  print('exponent = $exponent');
}
